#!/usr/bin/env python3
# vim:ts=4:sts=4:sw=4:expandtab

from distutils.core import setup

setup(
    name='python_pulseaudio',
    version='1.0',
    description='simple pulseaudio bindings',
    author='Grzegorz Gutowski',
    author_email='gutowski@tcs.uj.edu.pl',
    license='LGPL',
    url='http://bitbucket.org/grzegorz_gutowski/python_pulseaudio',
    packages=['pulseaudio'],
    provides=['python_pulseaudio'],
)
