#!/usr/bin/env python3
# vim:ts=4:sts=4:sw=4:expandtab

import sys
import wave

import pulseaudio as pa

(nchannels, sampformat, framerate, length) = (1, pa.SAMPLE_S16LE, 44100, 5.0)


with pa.simple.open(direction=pa.STREAM_RECORD, format=sampformat, rate=framerate, channels=nchannels) as recorder:
    wav = wave.open(sys.argv[1], 'wb')
    wav.setnchannels(recorder.channels)
    wav.setsampwidth(recorder.sample_width)
    wav.setframerate(recorder.rate)
    nframes = int(length * recorder.rate)
    try:
        count = 0
        while count < nframes:
            data = recorder.read(min(100,nframes-count))
            count += len(data)
            wav.writeframes(data.astype(recorder.sample_type).tostring())
    finally:
        wav.close()
