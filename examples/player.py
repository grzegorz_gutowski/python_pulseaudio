#!/usr/bin/env python3
# vim:ts=4:sts=4:sw=4:expandtab

import sys
import wave

import pulseaudio as pa
import numpy as np

sample_map = {
    1 : pa.SAMPLE_U8,
    2 : pa.SAMPLE_S16LE,
    4 : pa.SAMPLE_S32LE,
}

try:
    wav = wave.open(sys.argv[1], 'r')
    (nchannels, sampwidth, framerate, nframes, comptype, compname) = wav.getparams() 
    with pa.simple.open(direction=pa.STREAM_PLAYBACK, format=sample_map[sampwidth], rate=framerate, channels=nchannels) as player:
        while True:
            frames = wav.readframes(100)
            frames = np.fromstring(frames, dtype=player.sample_type).astype(np.float)
            if len(frames) == 0:
                break
            player.write(frames)
finally:
    wav.close()
