#!/usr/bin/env python3
# vim:ts=4:sts=4:sw=4:expandtab

import sys

import numpy as np
import pulseaudio as pa

(freq, framerate, amp) = (int(sys.argv[1]), 44100, 1000)

points = np.round(framerate/freq)
tone = amp*np.sin(np.r_[0:np.pi*2:np.pi*2/points])

with pa.simple.open(direction=pa.STREAM_PLAYBACK, format=pa.SAMPLE_S16LE, rate=framerate, channels=1) as player:
    while True:
        player.write(tone)
